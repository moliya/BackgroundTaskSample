//
//  AppDelegate.h
//  BackgroundTaskSample
//
//  Created by moliya on 16/4/22.
//  Copyright © 2016年 Wanquan Network Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (copy, nonatomic) void(^backgroundSessionCompletionHandler)();

@end

