//
//  CFURLSessionManager.m
//  BackgroundTaskSample
//
//  Created by moliya on 16/4/23.
//  Copyright © 2016年 Wanquan Network Technology. All rights reserved.
//

#import "CFURLSessionManager.h"

@implementation CFURLSessionManager

+ (instancetype)sharedSessionManager {
    static CFURLSessionManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[CFURLSessionManager alloc] init];
    });
    
    return manager;
}

@end
