//
//  CFURLSessionManager.h
//  BackgroundTaskSample
//
//  Created by moliya on 16/4/23.
//  Copyright © 2016年 Wanquan Network Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CFURLSessionManager : NSObject

+ (instancetype) sharedSessionManager;

@end
