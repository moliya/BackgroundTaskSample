//
//  fetchViewController.h
//  BackgroundTaskSample
//
//  Created by moliya on 16/4/23.
//  Copyright © 2016年 Wanquan Network Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, CFBackgroundUpdateResult) {
    CFBackgroundUpdateResultNewData,
    CFBackgroundUpdateResultNoData,
    CFBackgroundUpdateResultFailed,
};

@interface fetchViewController : UIViewController

- (void)fetchDataWithCompletionHandler:(void(^)(CFBackgroundUpdateResult result))completionHandler;

@end
